
#ifndef FRACTION_H
#define FRACTION_H
#include<iostream>
using namespace std;

class Fraction
{
public:
	Fraction();
	Fraction(int, int);
	friend Fraction operator+(const Fraction & a, const Fraction & b);
	friend Fraction operator-(const Fraction & a, const Fraction & b);
	friend Fraction operator*(const Fraction & a, const Fraction & b);
	friend Fraction operator/(const Fraction & a, const Fraction & b);
	void simplify(Fraction);
	void SEtNumi(int x);
	int GEtNumi();
	void SEtdeni(int y);
	int GEtdeni();
	bool operator<(Fraction);
	bool operator>(Fraction);
	bool operator==(Fraction);
	bool operator<=(Fraction);
	bool operator>=(Fraction);
	friend ostream &operator<<(ostream &output, const Fraction &temp);
	friend istream &operator>>(istream  &input, Fraction &temp);
	~Fraction();
protected:

	int numirator;
	int denimonitor;
	int numi_deni = numirator / denimonitor;
};

#endif // FRACTION_H
