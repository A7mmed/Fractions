#ifndef FRACTIONC_H
#define FRACTIONC_H
#include "Fraction.h"
#include<iostream>
using namespace std;


class FractionCalculator:public Fraction
{
public:

	FractionCalculator();
	void Addition(Fraction , Fraction);
	void Subtraction(Fraction, Fraction);
	void Multi(Fraction, Fraction);
	void Divi(Fraction, Fraction);


private:
    int n;
    int d;

};

#endif
